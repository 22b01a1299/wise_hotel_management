from tkinter import *

# Sample user credentials (you should replace these with a real authentication system).
user_credentials = {
    'user1': 'password1',
    'user2': 'password2',
}

class UserLogin:
    def __init__(self, root):
        self.root = root
        self.root.geometry('800x600')
        self.root.title('User Login and Navigation Example')

        self.background_image = PhotoImage(file='bg.png')  # Change 'background_image.png' to your image file.
        self.background_image = self.background_image.zoom(3)
        self.background_label = Label(root, image=self.background_image)
        self.background_label.place(relwidth=1, relheight=1)  # Make the label fill the entire window.

        self.navbar = self.create_navigation_bar()

        # Create a frame to hold the login elements
        self.login_frame = Frame(root, bg='white', width=300, height=200)
        self.login_frame.place(relx=0.5, rely=0.5, anchor='center')

        self.user_label = Label(self.login_frame, text="User Login", font=('Times New Roman', 16, 'bold'), bg='white')
        self.username_label = Label(self.login_frame, text="Username", font=('Times New Roman', 16), bg='white')
        self.username_entry = Entry(self.login_frame, font=('Times New Roman', 16))
        self.password_label = Label(self.login_frame, text="Password", font=('Times New Roman', 16), bg='white')
        self.password_entry = Entry(self.login_frame, show="*", font=('Times New Roman', 16))
        self.login_button = Button(self.login_frame, text="Login", font=('Times New Roman', 16), command=self.user_login)
        self.register_button = Button(self.login_frame, text="Register", font=('Times New Roman', 16), command=self.show_registration_window)

        self.login_result_label = Label(self.login_frame, text="", font=('Times New Roman', 16), bg='white')

        self.user_label.grid(row=0, column=0, columnspan=2, pady=10)
        self.username_label.grid(row=1, column=0, padx=10, pady=5, sticky='w')
        self.username_entry.grid(row=1, column=1, padx=10, pady=5)
        self.password_label.grid(row=2, column=0, padx=10, pady=5, sticky='w')
        self.password_entry.grid(row=2, column=1, padx=10, pady=5)
        self.login_button.grid(row=3, column=0, pady=10, padx=5)
        self.register_button.grid(row=3, column=1, pady=10, padx=5)
        self.login_result_label.grid(row=4, column=0, columnspan=2)

    def create_navigation_bar(self):
        nav_frame = Frame(self.root, bg='black')
        nav_frame.pack(fill='x', side='top', anchor='e')

        buttons = [
            "HOME", "ORDERS", "INVENTORY", "BILLING", "FEEDBACK", "ORDER ONLINE & DELIVERY"
        ]

        for button_text in reversed(buttons):
            button = Button(nav_frame, text=button_text, font=('Times New Roman', 16, 'bold'), bd=0, bg='black', fg='white')
            button.pack(side='right', padx=10, pady=10)

        return nav_frame

    def user_login(self):
        username = self.username_entry.get()
        password = self.password_entry.get()

        if username in user_credentials and user_credentials[username] == password:
            # Successful login
            self.login_result_label.config(text="Login Successful", fg="green")
            # Here, you can add code to navigate to the user's dashboard or perform other actions.
        else:
            # Failed login
            self.login_result_label.config(text="Login Failed. Invalid credentials.", fg="red")

    def show_registration_window(self):
        # You can create a registration window or dialog here to capture new customer details.
        # This may include username, password, and other required information.
        # Once the registration is complete, you can add the new user to your authentication system.
        pass

if __name__ == "__main__":
    root = Tk()
    user_login_app = UserLogin(root)
    root.mainloop()
